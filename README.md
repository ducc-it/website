# Sito web della DUCC-IT

Questo repository contiene il sito web della DUCC-IT. I contenuti sono
rilasciati sotto licenza Creative Commons BY-SA 4.0.

## Build del sito

Per fare una build del sito bisogna avere Python 3 installato. Dopo aver fatto
ciò, basta invocare make per eseguire la build:

```
$ make
```

La build verrà salvata in `build/html`. Lektor dispone anche di un server web
di sviluppo, con rebuild automatico e pannello di modifica dal browser. Per
avviarlo basta eseguire il comando:

```
$ make server
```

Infine, per pulire i file generati si può eseguire il comando:

```
$ make clean
```
