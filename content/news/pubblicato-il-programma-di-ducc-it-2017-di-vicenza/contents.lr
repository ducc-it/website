title: Pubblicato il programma di DUCC-IT 2017 di Vicenza
---
pub_date: 2017-04-19
---
body:

E’ stato pubblicato il [programma](/2017/programma/) della Debian Ubuntu
Community Conference 2017, che quest’anno si terrà sabato 6 e domenica 7 a
Vicenza, presso [Villa Lattes](/2017/location/).

Il programma è particolarmente ricco di spunti, che rappresentano il meglio
delle Comunità Debian e Ubuntu, ma anche delle comunità Mozilla Italia,
LibreItalia, Wikimedia e OpenStreetMap, accumunate dal tema di quest’anno
**Open communities for a better world** - comunità aperte per un mondo
migliore. I talk saranno ben sedici tenuti da [quattordici
relatori](/relatori/) provenienti da tutta Italia.

I talk affronteranno tre temi principali: utilizzo degli strumenti liberi,
esperienze di software libero sul campo e come contribuire a progetti liberi.
Tutti i talk saranno comunque accessibili al pubblico generico, che potrà
interagire con i relatori, grazie a uno spazio apposito destinato
all’approfondimento.

L’accesso all’evento è libero e gratuito. Per chi arriva da fuori Vicenza, sono
disponibili le informazioni su [come arrivare](/2017/location/) ed è stata
stipulata una convenzione con l’Hotel Verdi, ma sono disponibili [numerose
sistemazioni](/2017/dormire/) accessibili a tutte le tasche. Vi aspettiamo!
