title: Debian/Ubuntu Community Conference Italia 2017
---
pub_date: 2017-04-28
---
body:

La DUCC-IT 2017 si terrà a Vicenza il 6 e 7 maggio prossimi. Ubuntu-it, la
comunità italiana di Ubuntu, annuncia la Debian/Ubuntu Community Conference
Italia 2017 (in breve DUCC-IT 2017), l'incontro delle comunità italiane di
Debian e di Ubuntu in programma nei giorni 6 e 7 maggio 2017, [presso Villa
Lattes](/2017/location), in via Thaon di Ravel, Vicenza.

[Debian](https://www.debian.org) e [Ubuntu](https://www.ubuntu.com) sono tra le
distribuzioni più importanti nell’ambito dei sistemi operativi liberi basati su
GNU/Linux, e le rispettive comunità sono molto attive nel promuovere e
partecipare allo sviluppo dei due progetti. L’esperienza della DUCC-IT 2017,
che fa seguito alle edizioni tenutesi a Perugia, Fermo, Cesena e Milano, è
rivolta sia ai membri delle rispettive comunità che a tutti gli appassionati di
GNU/Linux e del Software Libero ed è un’ottima occasione per avvicinarsi e
conoscere meglio queste realtà e il Software Libero.

Il tema di DUCC-IT 2017 è “Open communities for a better world” (comunità
aperte per un mondo migliore), a sottolineare il generoso contributo che le
comunità hanno donato allo sviluppo e al supporto di GNU/Linux e del Software
Libero in tutto il mondo, diventando anche una concreta possibilità
occupazionale.

Il [programma](/2017/programma) è particolarmente ricco di spunti,
rappresentato dal meglio delle Comunità Debian e Ubuntu, ma anche delle
comunità Mozilla Italia, LibreItalia, Wikimedia e OpenStreetMap, accumunate dal
tema di quest’anno “Open communities for a better world” - comunità aperte per
un mondo migliore. I talk saranno ben sedici, tenuti da quindici relatori
provenienti da tutta Italia.

I talk in programma affronteranno tre filoni principali: utilizzo degli
strumenti liberi, esperienze di software libero sul campo e come contribuire a
progetti liberi. Il pubblico potrà interagire con i relatori, grazie a uno
spazio apposito destinato all’approfondimento.

Tra i numerosi ospiti confermati dell’evento figurano: [Marina
Latini](/relatori/marina-latini), Presidente di The Document Foundation che
sovrintende lo sviluppo della suite per ufficio LibreOffice, [Roberto
Guido](/relatori/roberto-guido) Presidente di Italian Linux Society che
organizza il Linux Day, [Marco Chemello](/relatori/marco-chemello) “wikipediano
in residenza” di BEIC (Biblioteca Europea di Informazione e Cultura), [Paolo
Dongilli](/relatori/paolo-dongilli), coordinatore del Progetto FUSS della
Provincia di Bolzano, per l’adozione di Software Libero nella scuola pubblica.

Uno spazio sarà dedicato all’adozione di Zorin OS da parte del Comune di
Vicenza, il sistema operativo basato su GNU/Linux che ha portato alla ribalta
il comune Veneto tra i pionieri dell’innovazione nell’ambito della Pubblica
Amministrazione.

L’evento si tiene presso Villa Lattes, ospiti del Comune di Vicenza, che ha
partecipato attivamente all’organizzazione dell’evento.

L’accesso all’evento è libero e gratuito. Per chi arriva da fuori Vicenza, sul
sito web dell’evento sono disponibili le informazioni su [come arrivare in auto
o con i mezzi pubblici](/2017/location). Per chi soggiorna a Vicenza, è stata
stipulata [una convenzione con l’Hotel Verdi](/2017/hotel).
