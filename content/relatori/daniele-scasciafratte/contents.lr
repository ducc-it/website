name: Daniele Scasciafratte
---
bio:

Daniele Scasciafratte è un developer italiano di 26 anni, con molta esperienza
in diversi progetti e comunità open source.
---
twitter: Mte90Net
---
website: https://mte90.net/ita/
---
quote:

Conoscere altre lingue è importante per poter rendere questo mondo, reso vicino
grazie ad internet, ancora più vicino.
---
interview:

**Chi sei?**

Sono nato e vivo a Rieti dove lavoro come sviluppatore, prettamente web, ed ho
una mia web agency a Roma di nome Codeat.

**Cosa fai?**

Mi occupo di sviluppo di siti e applicativi web e spesso faccio del lavoro da
sistemista.

**In che modo sei coinvolto con il Software Libero?**

Dal 2009 sono passato al software open e non sono più tornato indietro. Dal
2013 sono volontario [Mozilla](https://www.mozilla.org) e mi sono occupato
negli anni di organizzare e partecipare a molti eventi come relatore e nel
reclutamento di nuovi volontari.

Dal 2014 faccio parte del mondo [WordPress](https://www.wordpress.org) ed ora
sono Project Translation Editor oltre che Core Contributor di Wordpress. Sono
Club Captain del Mozilla Club Rieti e co-organizer del meetup WordPress Roma.
Sono Vice Presidente di [Industria Italiana del software
libero](http://www.industriasoftwarelibero.it/) che è una associazione dedicata
ai professionisti che usano, contribuiscono e promuovono software open source
in Italia.

**Quali sono le tue passioni?**

Oltre al mondo informatico sono un amante di fumetti, specialmente italiani ed
europei, che colleziono da quando ero un ragazzino.

Partecipando a conferenze internazionali per l'open source ho incominciato ad
imparare altre lingue, ed ora oltre all'inglese conosco l'esperanto e lo
spagnolo. Ritengo che conoscere altre lingue sia importante per poter rendere
questo mondo, reso vicino grazie ad internet, ancora più vicino con la
conoscenza di un’altra lingua.

**Di cosa parlerai a DUCC-IT 2017?**

Parlerò di [Mozilla Italia](/2017/programma/join-mozilla-we-have-cookies/) e
delle novità dell'ultimo anno come i Mozilla Club e i Mozilla Campus Club e per
un uso più coscienzioso di internet.

Con il secondo talk parlerò invece di
[Coaching](/2017/programma/coaching-for-open-source-communities/) che è una
tecnica o metodologia di approccio ai problemi nei gruppi di lavoro, nata in
ambito aziendale che ho scoperto nella mia militanza in Mozilla essere molto
utile per migliorare i rapporti e le attività dentro una comunità open source.

**Perché è importante esserci a DUCC-IT 2017?**

Perché sarà un luogo di incontro tra varie comunità, non solo Debian ed Ubuntu!
Un modo per incontrare molte persone sia di portata nazionale che locale ed
imparare.

**Perché sei ad un evento dedicato a Debian ed Ubuntu?**

Quando sono passato all'open ho scelto Debian Sid e lo utilizzo ancora oggi.
Nei miei primi passi nel mondo open ne ho fatto qualcuno anche traducendo le
descrizioni dei pacchetti Debian. Mi sento parte di questo gruppo perché sono
un utente Debian e quindi non potevo mancare a questa conferenza!
