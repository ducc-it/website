name: Andrea Lazzarotto
---
bio:

Sviluppatore software e consulente informatico, autore di
[RecuperaBit](https://github.com/Lazza/RecuperaBit/), amministratore di [Gimp
Italia](https://gimpitalia.it/), socio fondatore
[GrappaLUG](http://grappalug.org/), membro di [Ask
Ubuntu](https://askubuntu.com/).
---
twitter: thelazza
---
website: https://andrealazzarotto.com/
---
quote:

Ciò che si respira in molti eventi legati al software libero è l'energia e la
vitalità che si sviluppano dall'interazione vera e genuina tra appassionati.
---
interview:

**Chi sei?**

Sono Andrea Lazzarotto, classe 1991, utente Ubuntu da una decina d'anni. :)

**Cosa fai?**

Per professione mi occupo di sviluppo software e consulenza informatica.
Principalmente lavoro in ambito mobile/app e web, ma dipende dal singolo
progetto.

**In che modo sei coinvolto con il Software Libero?**

Il mio primo coinvolgimento è iniziato attorno al 2005, quando ho iniziato a
frequentare il forum di [Gimp Italia](https://gimpitalia.it/), di cui ora sono
amministratore. Non avevo ancora ben chiaro cosa fosse esattamente il sofware
libero. Ho cominciato ad interessarmene meglio alle scuole superiori,
partecipando al [LUG di Bassano del Grappa ](http://grappalug.org/) e fondando
il progetto Itis Linux per la diffusione del software libero negli istituti
tecnici industriali. Il progetto non ha avuto moltissimo successo, ma gli
eventi organizzati con il LUG sono sempre stati interessanti.  In seguito ho
cercato di portare qualche altro contributo a dei progetti esistenti,
segnalando bug e/o suggerimenti, o producendo traduzioni come nel caso di
[OpenShot](http://openshot.org/). Notando l'assenza di programmi di recupero
dati avanzati per NTFS su Linux ho deciso di realizzare
[RecuperaBit](https://github.com/Lazza/RecuperaBit/), progetto interamente
rilasciato sotto licenza GPL.

Attualmente mi occupo principalmente di mantenere i miei piccoli script open
source per salvare video dalle principali reti televisive (Rai, Mediaset, La7,
RSI, BBC) e quando posso cerco di aggiungere qualche funzione a RecuperaBit. Mi
piace collaborare con la community di [Ask Ubuntu](https://askubuntu.com), un
sito web dedicato a tutti gli utenti di Ubuntu che desiderano darsi una mano a
vicenda per risolvere qualsiasi dubbio.

Ogni tanto capita di inviare delle patch a dei progetti che uso per lavoro,
come ad esempio [Apache Cordova](https://cordova.apache.org/).

**Quali sono le tue passioni?**

Il karate, gli algoritmi, il sushi, le uscite al pub, i fumetti, i thriller...

**Di cosa parlerai a DUCC-IT 2017?**

A DUCC-IT 2017 [esporrò una breve
presentazione](/2017/programma/aiutare-anche-senza-saper-programmare/) su Ask
Ubuntu, partendo dall'introdurre che cos'è questa community fino a spiegare
come tutti gli utenti Ubuntu possano portare il proprio contributo anche senza
dover essere sviluppatori software o saper programmare.

**Perché è importante esserci a DUCC-IT 2017?**

Siamo una società sempre più connessa "virtualmente" ma meno connessa nella
vita reale. Eppure la comunità del software libero è fatta di persone in carne
ed ossa, tutte fantastiche e che meritano di essere conosciute. Credo che la
cosa più bella di partecipare alla DUCC-IT 2017 non sia tanto vedere il talk
quanto incontrare altre persone con cui poter scambiare spunti e idee, o anche
solo fare due chiacchiere. Questo è ciò che si respira in molti eventi legato
al software libero: l'energia e la vitalità che si sviluppano dall'interazione
vera e genuina tra appassionati di FLOSS sono pressoché impossibili da
riprodurre "incontrandosi" solo online.
