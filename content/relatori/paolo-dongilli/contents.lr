name: Paolo Dongilli
---
bio:

Coordinatore [Progetto FUSS](https://www.fuss.bz.it), Intendenza Scolastica
Italiana, Provincia Autonoma di Bolzano
---
twitter: opavlos
---
quote:

Ogni lavoro mi ha visto in prima linea nell'uso del Software Libero per la
digitalizzazione sostenibile, su tre pilastri: Software Libero, standard aperti
e contenuti liberi per un libero accesso al sapere.
---
interview:

**Chi sei?**

Paolo Dongilli (the man with a white cat on his shoulder). Sono nato e vivo a
Bolzano con mia moglie e due bimbi di 2 e 6 anni. Appassionato di informatica
dall'età di 11 anni non riesco a stare un giorno senza un computer in modalità
"more magic".

**Cosa fai?**

Lavoro in qualità di ispettore tecnico presso l'Intendenza Scolastica Italiana
della Provincia Autonoma di Bolzano e coordino il [progetto
FUSS](https://www.fuss.bz.it) (Free Upgrade in South Tyrol's Schools) che 12
anni fa, nel 2005, ha portato GNU/Linux e Software Libero in tutte le scuole in
lingua italiana della provincia.

**In che modo sei coinvolto con il Software Libero?**

Negli anni '90 durante il periodo universitario ho compreso il significato e
l'importanza di utilizzare e far conoscere il Software Libero (di seguito SL)
nel quotidiano. Nel 2001 con un gruppo di amici abbiamo fondato il primo LUG
locale ([LUGBZ](https://www.lugbz.org)) con il quale fino ad oggi abbiamo
condiviso numerose esperienze ed organizzato svariati eventi per la
cittadinanza.

Ogni posto di lavoro che ho ricoperto mi ha visto in prima linea
nell'incentivazione all'uso del SL per un'idea di digitalizzazione sostenibile
che vedesse tra i suoi pilastri oltre al SL stesso anche l'utilizzo di standard
aperti, l'utilizzo di contenuti liberi ed un libero accesso al sapere.

Sono attualmente membro di The Document Foundation, certificato come
LibreOffice Migration Professional in un periodo sfavorevole, in cui le nostre
PA locali hanno deciso di migrare a Office 365. Membro fondatore
dell'associazione [LibreItalia onlus](http://www.libreitalia.it) e Associate
Member della [Free Software Foundation](https://www.fsf.org). Dal 2016 sono
membro fondatore del gruppo [Digital Sustainability Südtirol-Alto
Adige](https://www.openbz.eu) che è partito creando un manifesto di
sostenibilità digitale scaricabile dal sito. Organizziamo incontri stagionali
denominati "colloqui" risvegliando un notevole interesse non solo a livello
locale ma anche fuori dai confini della nostra provincia. Ora, sulla scia di
quanto il LinuxTrent sta facendo con successo da 5 anni, anche il gruppo di
Sostenibilità Digitale assiem al LUGBZ. Con il sostegno del Consiglio di
quartiere Europa-Novacella del Comune di Bolzano e della Provincia Autonoma di
Bolzano siamo partiti con uno Sportello Open & Linux. Offriamo ai cittadini
supporto all'installazione di GNU/Linux e SL, guidandoli nell'utilizzo
quotidiano anche mediante una serie di workshop tematici.

**Quali sono le tue passioni?**

Con due bimbi piccoli l'unica passione che riesco a coltivare nei ritagli di
tempo dopo la famiglia ed il lavoro è il volontariato informatico. Non
chiedetemi però di installare finestre o ripararne di rotte. L'unica cosa che
posso fare è aiutarvi a fare un po' di pulizia.

**Di cosa parlerai a DUCC-IT 2017?**

Al DUCC-IT 2017 presenterò il progetto FUSS, la sua storia dodecennale
in pillole, l'attuale processo di migrazione alla nuova release basata
su Debian e le future attività.

**Perché è importante esserci a DUCC-IT 2017?**

L'importanza di partecipare a DUCC-IT è data dalla ricchezza che
deriva dal conoscere e venire a far parte di una rete di persone unite
dalla stessa passione ed incontrare una parte della comunità nazionale
che lavora e persegue lo stesso obiettivo: rendere trasparente
l'utilizzo delle tecnologie informatiche in un contesto di massima
condivisione della conoscenza.
